#if INTERACTIVE
#r @"FSharp.Data.dll"
#endif

open System
open System.IO
open System.Net.Sockets
open FSharp.Data

// Sample files should be the same in both directories
//type Json = JsonProvider< @"d:\Personal\GitHub\HelloWorldOpen\fsharp\sample.json", SampleIsList=true, RootName="message">
type Json = JsonProvider<"./sample.json", SampleIsList=true, RootName="message">

type Direction = Left|Right
type HelloWorldOpenClient(host, port) = 
    let getCurrentDateString() = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")
    let gameName = ref (getCurrentDateString() + ".txt")
    let log (msg:Json.Message) =
    #if INTERACTIVE
        let logDirectory = @"d:\Personal\GitHub\HelloWorldOpen\fsharp\logs\";
        let logFile = Path.Combine(logDirectory, !gameName)
        let message = sprintf "%A" msg
        File.AppendAllText(logFile, message, Text.Encoding.UTF8)
    #else
        ignore()
    #endif
    let client = new TcpClient(host, port)
    let stream = client.GetStream()
    let reader = new StreamReader(stream)
    let writer = new StreamWriter(stream)
    let gameTick = ref None // gameTick from last received message
    let send (msg : Json.Message) =
        log msg
        writer.WriteLine (msg.JsonValue.ToString(JsonSaveOptions.DisableFormatting))
        writer.Flush()
    let getJoinData color key name botId trackName carCount = 
        Json.Data(
            color = color,
            key = key, 
            name = name,
            race = None,
            bestLaps = null, 
            results = null,
            car = None,
            lapTime = None,
            raceTime = None,
            ranking = None, 
            reason = None, 
            turboDurationMilliseconds = None,
            turboDurationTicks = None, 
            turboFactor = None,
            botId = botId,
            carCount = carCount,
            password = None,
            trackName = trackName)
    interface IDisposable with
        member this.Dispose() = 
            writer.Dispose()
            reader.Dispose()
            client.Close()
    // Get Message
    member this.getMessage () =
        match (reader.ReadLine()) with
        | null -> None
        | line ->
            let msg = Json.Parse(line)
            log msg
            gameTick := msg.GameTick
            Some(msg)
    // Join to the Game
    member this.join name key color =
        gameName := sprintf "%s - join.txt" (getCurrentDateString())
        Json.Message(
            msgType = "join",
            data = Json.DecimalOrStringOrArrayOrData(
                    getJoinData (Some(color)) (Some(key)) (Some(name)) None None None),
            gameId = None,
            gameTick = !gameTick)
        |> send
    member this.joinRace name key trackName carCount = 
        gameName := sprintf "%s - joinRace(%s,%d).txt" (getCurrentDateString()) trackName carCount
        Json.Message(
            msgType = "joinRace",
            data = Json.DecimalOrStringOrArrayOrData(
                    getJoinData None None None (Some(Json.BotId(name,key))) (Some(trackName)) (Some(carCount))),
            gameId = None,
            gameTick = None)
        |> send
    // Bot Messages
    member this.ping () =
        Json.Message(
            msgType = "ping", 
            data = null,
            gameId = None,
            gameTick = !gameTick)
        |> send
    member this.throttle (value:Decimal) =
        printfn "throttle = %f" value
        Json.Message(
            msgType = "throttle", 
            data = Json.DecimalOrStringOrArrayOrData(value),
            gameId = None,
            gameTick = !gameTick)
        |> send
    member this.switchLane (direction:Direction) =
        Json.Message(
            msgType = "switchLane", 
            data = Json.DecimalOrStringOrArrayOrData(sprintf "%A" direction),
            gameId = None,
            gameTick = !gameTick)
        |> send
    member this.turbo () =
        Json.Message(
            msgType = "turbo", 
            data = Json.DecimalOrStringOrArrayOrData("Pow pow pow"),
            gameId = None,
            gameTick = !gameTick)
        |> send
            
[<EntryPoint>]
let main args =
#if INTERACTIVE
    let botName, botKey = "RAVENT", "ajni3lirKnJcmw"
    let port = 8091
    let hosts = 
        ["testserver.helloworldopen.com"
         "hakkinen.helloworldopen.com"
         "senna.helloworldopen.com"
         "webber.helloworldopen.com"]
    let host = hosts.[1]
#else
    let (host, portString, botName, botKey) = 
      args |> (function [|a;b;c;d|] -> a, b, c, d | _ -> failwith "Invalid param array")
    let port = Convert.ToInt32(portString)
#endif


    printfn "Connecting to %s:%d as %s/%s" host port botName botKey

    use client = new HelloWorldOpenClient(host, port)

    client.join botName botKey "blue" // max speed 0.65m
    //client.joinRace botName botKey "germany" 1 // max speed 0.4m
    //client.joinRace botName botKey "usa" 1 // max speed 1m

    let processNextLine = function
        | None -> false
        | Some(msg:Json.Message) ->
            try 
                printfn "msg = %A; gameTick = %A" // TODO: delete it
                    msg.MsgType msg.GameTick
                match msg.MsgType with
                | "carPositions" -> client.throttle 0.65m
                | "join" -> printfn "Joined"; client.ping()
                //| "gameInit" -> printfn "Race init"; client.ping()
                //| "gameEnd" -> printfn "Race ends"; client.ping()
                //| "gameStart" -> printfn "Race starts"; client.ping()
                | _ -> 
                    printfn "%A" msg
                    client.ping()
                true
            with
            | ex -> true

    while processNextLine (client.getMessage()) do 
        ()

    0